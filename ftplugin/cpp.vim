" map <F5> :!clear<CR> :!g++ -g %<CR>
" map <F5> :!clear<CR> :!./compile.sh<CR>
" map <F5> :silent !tmux send-keys -t right.0 'bash ~/scripts/buildTestBody.sh' ENTER<CR> <c-l>
map <F5> :w<CR> :silent !tmux send-keys -t right.0 'q' <CR> :silent !tmux send-keys -t right.0 c-c <CR> :silent !tmux send-keys -t right.0 'bash ~/scripts/buildTestBody.sh' ENTER<CR> <c-l>
map <leader>b :w<CR> :silent !tmux send-keys -t right.0 'q' <CR> :silent !tmux send-keys -t right.0 c-c <CR> :silent !tmux send-keys -t right.0 'bash ~/scripts/buildTestBody.sh' ENTER<CR> <c-l>

map <leader>s :if exists("g:syntax_on") <Bar>
	\   syntax off <Bar>
	\ else <Bar>
	\   syntax enable <Bar>
	\ endif <CR>

imap ;, (),
imap ;c<Space> const 
imap ;s<Space> std::
imap ;e<Space> Eigen::
imap ;vev std::vector<Eigen::Vector3f>
imap ;sv std::vector<><ESC>i
imap ;sp std::pair<><ESC>i
imap ;sum std::unordered_map<><ESC>i
imap ;ev Eigen::Vector3f
imap ;el std::endl
imap ;ut uint32_t
imap ;st size_t
imap ;csv const std::vector<> <ESC>i
imap ;csvr const std::vector<>& <ESC>hi
imap ;cev const Eigen::Vector3f
imap ;cel const std::endl
imap ;cut const uint_32t
imap ;cst const size_t
imap ;cevr const Eigen::Vector3f&
imap ;cutr const uint_32t&
imap ;cstr const size_t&
imap ;for for(int i=0; i<n; ++i)
imap ;cout std::cout << "v:\n" << v << std::endl;
imap ;tran std::transform(std::cbegin(src), std::cend(src), std::back_inserter(dst), [&](const auto& S) {return S;});
imap ;r<Space> &
imap ;fcout std::cout << "Vec" << std::cout; for (auto E : Vec) std::cout << E << ", "; std::cout<<std::endl;
