source ~/.vim/biggreplocal.vim
hi Folded ctermbg=black
" let g:pymode_python = 'python3'
inoremap jk <ESC>
" set ttimeoutlen=50
set hidden
set timeoutlen=1000 ttimeoutlen=0
set swapfile
set dir=~/tmp
set number
set relativenumber
set tabstop=4
set path=.,/usr/local/include,/usr/include,/home/tangchengcheng/ovrsource/Research/human-tracking/nimble,/home/tangchengcheng/ovrsource/Research/human-tracking/nimble/core,/home/tangchengcheng/ovrsource/Research/human-tracking/nimble/tools,/home/tangchengcheng/ovrsource/Research/human-tracking/nimble/experimental,/home/tangchengcheng/ovrsource/Research/human-tracking/nimble/sdk,/home/tangchengcheng/ovrsource/Research/human-tracking/nimble/tests
let mapleader = " "
let maplocalleader = "\\"
" Search for tags recursively
set tags+=./tags;$HOME
" Open the definition in a new tab
map <C-\> :tab split<CR>:exec("tag ".expand("<cword>"))<CR>
map <A-]> :vsp <CR>:exec("tag ".expand("<cword>"))<CR>
map <C-b> <Nop>
map <leader>b <C-^>
map <leader>v :tabe ~/.vim/vimrc<CR>
map <leader>z :FZF<CR>
map <leader>c :tabe ~/.vim/ftplugin/cpp.vim<CR>
map <leader>p :tabe ~/.vim/ftplugin/python.vim<CR>

set backspace=indent,eol,start
noremap  <buffer> <silent> k gk
noremap  <buffer> <silent> j gj
noremap  <buffer> <silent> 0 g0
noremap  <buffer> <silent> $ g$
set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab

" REQUIRED. This makes vim invoke Latex-Suite when you open a tex file.
filetype plugin on
syntax on

" IMPORTANT: win32 users will need to have 'shellslash' set so that latex
" can be called correctly.
set shellslash

" IMPORTANT: grep will sometimes skip displaying the file name if you
" search in a singe file. This will confuse Latex-Suite. Set your grep
" program to always generate a file-name.
set grepprg=grep\ -nH\ $*

" OPTIONAL: This enables automatic indentation as you type.
filetype indent on

" OPTIONAL: Starting with Vim 7, the filetype of empty .tex files defaults to
" 'plaintex' instead of 'tex', which results in vim-latex not being loaded.
" The following changes the default filetype back to 'tex':
let g:tex_flavor='latex'

" set number

set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab

map <F6> :!clear<CR> :!g++ % -o a.out -g -fopenmp -std=c++11 <CR> :!./a.out <CR>

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=/home/tangchengcheng/.vim/bundle/Vundle.vim
" set rtp+=/Users/chengcheng/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'w0rp/ale'
Plugin 'octol/vim-cpp-enhanced-highlight'
Plugin 'flazz/vim-colorschemes'
Plugin 'felixhummel/setcolors.vim'
Plugin 'altercation/vim-colors-solarized'
Plugin 'jelera/vim-javascript-syntax'
Plugin 'kh3phr3n/python-syntax'
Plugin 'morhetz/gruvbox'
Plugin 'vim-perl/vim-perl'
Plugin 'davidhalter/jedi'
Plugin 'jceb/vim-orgmode'
let g:ycm_filepath_completion_use_working_dir = 1
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'nvie/vim-flake8'
let g:flake8_cmd="/usr/local/bin/flake8"
map <F7> :call flake8#Flake8() <CR>
Plugin 'mileszs/ack.vim'
nnoremap <Leader>a :Ack!<Space>
let g:ackpreview = 1
set directory=~/.vimtmp
Plugin 'Valloric/YouCompleteMe'
let g:ycm_global_ycm_extra_conf = '~/nfs/global_extra_conf.py'
" let g:ycm_global_ycm_extra_conf = '~/fbsource/fbcode/experimental/wmills/ycm/ycm_extra_conf_fbcode.py'
let g:ycm_filetype_blacklist = {
            \ 'javascript': 1,
            \ 'js': 1}
nnoremap <leader>g :YcmCompleter GoTo<CR>
"let g:ycm_python_binary_path = 'python'
let g:ycm_add_preview_to_completeopt = 1
Plugin 'Chiel92/vim-autoformat'
Plugin 'rhysd/vim-clang-format'

let Tlist_Ctags_Cmd='ctags'
let Tlist_Show_One_File=1               "不同时显示多个文件的tag，只显示当前文件的
let Tlist_WinWidt =28                                   "设置taglist的宽度
let Tlist_Exit_OnlyWindow=1             "如果taglist窗口是最后一个窗口，则退出vim

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
let g:clang_format#coding_style = 'llvm'
let g:clang_format#style_options = {
            \ "AccessModifierOffset" : -4,
            \ "AllowShortIfStatementsOnASingleLine" : "true",
            \ "AlwaysBreakTemplateDeclarations" : "true",
            \ "Standard" : "C++11",
            \ "ColumnLimit": 80}
map <leader>f :Autoformat<CR>

set rtp+=~/.fzf
" This is the default extra key bindings
let g:fzf_action = {
            \ 'ctrl-t': 'tab split',
            \ 'ctrl-x': 'split',
            \ 'ctrl-v': 'vsplit' }

" Default fzf layout
" - down / up / left / right
let g:fzf_layout = { 'down': '~40%' }

" In Neovim, you can set up fzf window using a Vim command
let g:fzf_layout = { 'window': 'enew' }
let g:fzf_layout = { 'window': '-tabnew' }
let g:fzf_layout = { 'window': '10split enew' }

" Customize fzf colors to match your color scheme
let g:fzf_colors =
            \ { 'fg':      ['fg', 'Normal'],
            \ 'bg':      ['bg', 'Normal'],
            \ 'hl':      ['fg', 'Comment'],
            \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
            \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
            \ 'hl+':     ['fg', 'Statement'],
            \ 'info':    ['fg', 'PreProc'],
            \ 'border':  ['fg', 'Ignore'],
            \ 'prompt':  ['fg', 'Conditional'],
            \ 'pointer': ['fg', 'Exception'],
            \ 'marker':  ['fg', 'Keyword'],
            \ 'spinner': ['fg', 'Label'],
            \ 'header':  ['fg', 'Comment'] }

" fzf Select buffer
function! s:buflist()
    redir => ls
    silent ls
    redir END
    return split(ls, '\n')
endfunction

function! s:bufopen(e)
    execute 'buffer' matchstr(a:e, '^[ 0-9]*')
endfunction

nnoremap <silent> <Leader><Enter> :call fzf#run({
            \   'source':  reverse(<sid>buflist()),
            \   'sink':    function('<sid>bufopen'),
            \   'options': '+m',
            \   'down':    len(<sid>buflist()) + 2
            \ })<CR>

" Enable per-command history.
" CTRL-N and CTRL-P will be automatically bound to next-history and
" previous-history instead of down and up. If you don't like the change,
" explicitly bind the keys to down and up in your $FZF_DEFAULT_OPTS.
let g:fzf_history_dir = '~/.local/share/fzf-history'

set t_Co=256
set background=dark
colo solarized
let g:solarized_contrast = 'high'
let g:solarized_bold = 0
call togglebg#map("<F12>")

" https://github.com/spf13/spf13-vim/issues/540
set shortmess=a
set cmdheight=2

" https://vi.stackexchange.com/questions/2129/fastest-way-to-switch-to-a-buffer-in-vim
nnoremap <Leader>l :ls<CR>:b<Space>
